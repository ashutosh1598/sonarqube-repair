/**
 * Ready to go meta?
 * This package contains classes that model the Spoon metamodel itself.
 * So this is the metametamodel.
 */
package spoon.metamodel;

